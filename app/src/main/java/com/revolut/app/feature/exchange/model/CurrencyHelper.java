package com.revolut.app.feature.exchange.model;

public abstract class CurrencyHelper {

  public static final String EUR_CODE = "EUR";
  public static final String USD_CODE = "USD";
  public static final String GBP_CODE = "GBP";

}
