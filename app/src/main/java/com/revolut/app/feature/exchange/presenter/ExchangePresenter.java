package com.revolut.app.feature.exchange.presenter;

import android.os.Bundle;

import com.revolut.app.R;
import com.revolut.app.core.Utils;
import com.revolut.app.feature.exchange.Contract;
import com.revolut.app.feature.exchange.model.CurrencyHelper;

import java.util.Observer;

public class ExchangePresenter implements Contract.Presenter{

  private static final int PAGES_COUNT = 3;

  public static final String KEY_INPUT = "KEY_INPUT:";

  private static String[] CURRENCY_CODES = {CurrencyHelper.EUR_CODE, CurrencyHelper.USD_CODE, CurrencyHelper.GBP_CODE};

  private final Contract.View mView;
  private final Contract.Model mModel;

  private Double[] mInputs = new Double[PAGES_COUNT];
  private int mInputIndex;
  private int mOutputIndex;
  private final Observer mOnCurrencyRatesChanged = (observable, data) -> getView().updateOutputView();

  public ExchangePresenter(Contract.View view, Contract.Model model) {
    mView = view;
    mModel = model;
  }

  @Override
  public Contract.View getView() {
    return mView;
  }

  @Override
  public void start() {
    mModel.subscribeOnCurrencyRatesChanged(mOnCurrencyRatesChanged);
  }

  @Override
  public void stop() {
    mModel.unSubscribeOnCurrencyRatesChanged(mOnCurrencyRatesChanged);
  }

  @Override
  public void restoreState(Bundle bundle) {
    for (int i = 0; i < CURRENCY_CODES.length; i++) {
      String currencyCode = CURRENCY_CODES[i];
      double aDouble = bundle.getDouble(KEY_INPUT + currencyCode);
      boolean isNull = aDouble < 0.00001;
      mInputs[i] = !isNull ? aDouble : null;
    }
  }

  @Override
  public void storeState(Bundle bundle) {
    for (int i = 0; i < CURRENCY_CODES.length; i++) {
      String currencyCode = CURRENCY_CODES[i];
      Double value = mInputs[i];
      if (value != null) {
        bundle.putDouble(KEY_INPUT + currencyCode, value);
      }
    }
  }

  @Override
  public void onInputIndexChanged(int index) {
    mInputIndex = index;
    mView.updateOutputView();
  }

  @Override
  public void onInputAmountChanged(String input) {
    try {
      mInputs[mInputIndex] = Double.parseDouble(input);
    } catch (NumberFormatException e) {
      mView.showBadInputNotificationText();
      mInputs[mInputIndex] = null;
    } finally {
      mView.updateOutputView();
    }
  }

  @Override
  public int getPagesCount() {
    return PAGES_COUNT;
  }

  @Override
  public void onOutputIndexChanged(int index) {
    mOutputIndex = index;
    mView.updateOutputView();
  }

  @Override
  public String getInputCurrencyCode() {
    return CURRENCY_CODES[mInputIndex];
  }

  @Override
  public Double getInputValue() {
    return mInputs[mInputIndex];
  }

  @Override
  public Double getInputValueForCurrencyCode(String currencyCode) {
    return mInputs[Utils.indexOf(CURRENCY_CODES, currencyCode)];
  }

  @Override
  public Double getUserBalance(String currencyCode) {
    return mModel.getWalletCurrencyAmount(currencyCode);
  }

  @Override
  public Double getCurrencyExchangeRate(String fromCurrencyCode, String toCurrencyCode) {
    return mModel.getCurrencyExchangeRate(fromCurrencyCode, toCurrencyCode);
  }

  @Override
  public String getCurrencyCodeForPosition(int position) {
    return CURRENCY_CODES[position];
  }
}
