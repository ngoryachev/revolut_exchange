package com.revolut.app.feature.exchange.presenter;

public class PagerState {

  private String currencyCode;
  private Double currencyBalance;
  private Double currencyValue;
  private Double currencyExchangeRate;

  public PagerState() {}

  public PagerState(String currencyName, Double currencyBalance, Double currencyValue, Double currencyExchangeRate) {
    this.currencyCode = currencyName;
    this.currencyBalance = currencyBalance;
    this.currencyValue = currencyValue;
    this.currencyExchangeRate = currencyExchangeRate;
  }

  public String getCurrencyCode() {
    return currencyCode;
  }

  public void setCurrencyCode(String currencyCode) {
    this.currencyCode = currencyCode;
  }

  public Double getCurrencyBalance() {
    return currencyBalance;
  }

  public void setCurrencyBalance(Double currencyBalance) {
    this.currencyBalance = currencyBalance;
  }

  public Double getCurrencyValue() {
    return currencyValue;
  }

  public void setCurrencyValue(Double value) {
    this.currencyValue = value;
  }

  public Double getCurrencyExchangeRate() {
    return currencyExchangeRate;
  }

  public void setCurrencyExchangeRate(Double currencyExchangeRate) {
    this.currencyExchangeRate = currencyExchangeRate;
  }
}
