package com.revolut.app.core;

public interface BaseView<P extends BasePresenter> {

  P getPresenter();

}
