package com.revolut.app.feature.exchange.view;


import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;

import com.revolut.app.R;

public class CurrencyPageEditableFragment extends CurrencyPageFragment{

  protected CurrencyItemViewEditable mInputItemView;

  @Override
  protected int getLayout() {
    return R.layout.currency_layout_editable;
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    View view = onCreateViewBasic(inflater, container);
    mInputItemView = (CurrencyItemViewEditable) mItemViewRight;

    bindView();

    mInputItemView.getInput().addTextChangedListener(new TextWatcher() {

      @Override
      public void beforeTextChanged(CharSequence s, int start, int count, int after) {
      }

      @Override
      public void onTextChanged(CharSequence s, int start, int before, int count) {
      }

      @Override
      public void afterTextChanged(Editable s) {
        getPresenter().onInputAmountChanged(s.toString());
      }

    });
    return view;
  }

  @Override
  protected void bindView() {
    bindLeftSide();

    String currencyCode = getCurrencyCode();
    Double inputValueForCurrencyCode = getPresenter().getInputValueForCurrencyCode(currencyCode);
    String text = formatCurrencyValue(inputValueForCurrencyCode);
    EditText input = mInputItemView.getInput();
    input.setText(text);
    input.setSelection(input.getText().length());
  }
}
