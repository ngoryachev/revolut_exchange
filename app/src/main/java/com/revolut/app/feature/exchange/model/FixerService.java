package com.revolut.app.feature.exchange.model;

import io.reactivex.Observable;
import retrofit2.http.GET;
import retrofit2.http.Query;

public interface FixerService {

  @GET("latest?")
  Observable<CurrencyRates> getCurrencyRates(@Query("base") String base);

}