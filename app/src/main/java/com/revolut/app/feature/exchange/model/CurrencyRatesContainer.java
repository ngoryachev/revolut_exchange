package com.revolut.app.feature.exchange.model;

import com.google.gson.Gson;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;
import java.util.concurrent.ConcurrentHashMap;

public class CurrencyRatesContainer {

  @SerializedName("currencyRatesMap")
  @Expose
  private Map<String, CurrencyRates> currencyRatesMap = new ConcurrentHashMap<>();

  public CurrencyRates getCurrencyRates(String currency) {
    return currencyRatesMap.get(currency);
  }

  public void setCurrencyRates(String currency, CurrencyRates rates) {
    currencyRatesMap.put(currency, rates);
  }

  public String toJsonString() {
    Gson gson = new Gson();
    return gson.toJson(this);
  }

  public CurrencyRatesContainer fromJsonString(String json) {
    Gson gson = new Gson();
    return gson.fromJson(json, CurrencyRatesContainer.class);
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    CurrencyRatesContainer that = (CurrencyRatesContainer) o;

    return currencyRatesMap != null ? currencyRatesMap.equals(that.currencyRatesMap) : that.currencyRatesMap == null;

  }

  @Override
  public int hashCode() {
    return currencyRatesMap != null ? currencyRatesMap.hashCode() : 0;
  }
}
