package com.revolut.app.feature.exchange.view;

import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.View;

import com.revolut.app.App;
import com.revolut.app.R;
import com.revolut.app.core.Utils;
import com.revolut.app.feature.exchange.Contract;
import com.revolut.app.feature.exchange.presenter.ExchangePresenter;
import com.trello.navi2.component.support.NaviAppCompatActivity;

import java.util.Observable;
import java.util.Observer;

public class ExchangeActivity extends NaviAppCompatActivity implements Contract.View{

  private Contract.Presenter mPresenter;
  private final Observable mInputObservable = new Utils.SimpleObservable();

  @Override
  protected void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.activity_main);

    mPresenter = new ExchangePresenter(this, ((App)getApplication()).getExchangeModel());

    if (savedInstanceState != null) {
      mPresenter.restoreState(savedInstanceState);
    }

    createViewPager(true, R.id.exchange_layout_pager_top, R.id.exchange_layout_tab_top);
    createViewPager(false, R.id.exchange_layout_pager_bottom, R.id.exchange_layout_tab_bottom);
  }

  @Override
  protected void onSaveInstanceState(Bundle outState) {
    super.onSaveInstanceState(outState);
    mPresenter.storeState(outState);
  }

  @Override
  protected void onStart() {
    super.onStart();
    mPresenter.start();
  }

  @Override
  protected void onStop() {
    super.onStop();
    mPresenter.stop();
  }

  private ViewPager createViewPager(boolean inputEnabled, int viewPagerId, int tabLayoutId) {
    ViewPager viewPager = (ViewPager) findViewById(viewPagerId);
    viewPager.setAdapter(createPagerAdapter(inputEnabled));
    TabLayout tabLayout = (TabLayout) findViewById(tabLayoutId);
    tabLayout.setupWithViewPager(viewPager);
    viewPager.addOnPageChangeListener(new ViewPager.SimpleOnPageChangeListener(){
      @Override
      public void onPageSelected(int position) {
        if (inputEnabled) {
          mPresenter.onInputIndexChanged(position);
        } else {
          mPresenter.onOutputIndexChanged(position);
        }
      }
    });

    return viewPager;
  }

  private CurrencyPagerAdapter createPagerAdapter(boolean inputEnabled) {
    return new CurrencyPagerAdapter(getSupportFragmentManager(), inputEnabled);
  }

  @Override
  public Contract.Presenter getPresenter() {
    return mPresenter;
  }

  @Override
  public void updateOutputView() {
    mInputObservable.notifyObservers();
  }

  @Override
  public void subscribeInputChanged(Observer observer) {
    mInputObservable.addObserver(observer);
  }

  @Override
  public void unsubscribeInputChanged(Observer observer) {
    mInputObservable.deleteObserver(observer);
  }

  private void showNotificationText(String text) {
    View view = findViewById(R.id.activity_main_coordinator_layout);
    Snackbar.make(view, text, Snackbar.LENGTH_LONG).show();
  }

  @Override
  public void showBadInputNotificationText() {
    showNotificationText(getString(R.string.please_type_valid_decimal));
  }

  private class CurrencyPagerAdapter extends FragmentStatePagerAdapter {

    private final boolean mInputEnabled;

    public CurrencyPagerAdapter(FragmentManager fm, boolean inputEnabled) {
      super(fm);
      mInputEnabled = inputEnabled;
    }

    @Override
    public Fragment getItem(int position) {
      return CurrencyPageFragment.create(mInputEnabled, getPresenter().getCurrencyCodeForPosition(position));
    }

    @Override
    public int getCount() {
      return mPresenter.getPagesCount();
    }
  }
}
