package com.revolut.app.feature.exchange.model;

import android.support.annotation.Nullable;

import java.util.HashMap;
import java.util.Map;

public class Wallet {

  private Map<String, Double> mCurrencyAmountMap = new HashMap<>();

  public Wallet() {
    mCurrencyAmountMap.put(CurrencyHelper.EUR_CODE, 100.0);
    mCurrencyAmountMap.put(CurrencyHelper.USD_CODE, 100.0);
    mCurrencyAmountMap.put(CurrencyHelper.GBP_CODE, 100.0);
  }

  public @Nullable Double getAmount(String currency) {
    return mCurrencyAmountMap.get(currency);
  }

}
