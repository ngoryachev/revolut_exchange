package com.revolut.app.feature.exchange.view;

import android.content.Context;
import android.util.AttributeSet;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.revolut.app.R;

public class CurrencyItemView extends FrameLayout {

  protected LinearLayout mLinearLayout;
  protected TextView mPrimary;
  protected TextView mSecondary;

  public CurrencyItemView(Context context) {
    super(context);
    initialize(context);
  }

  public CurrencyItemView(Context context, AttributeSet attrs) {
    super(context, attrs);
    initialize(context);
  }

  public CurrencyItemView(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    initialize(context);
  }

  private void initialize(Context context) {
    LayoutInflater.from(context).inflate(getLayout(), this);
    mLinearLayout = (LinearLayout) this.getChildAt(0);
    mPrimary = (TextView) mLinearLayout.getChildAt(0);
    mSecondary = (TextView) mLinearLayout.getChildAt(1);
  }

  public void setRowsGravityLeft() {
    setRowsGravity(Gravity.CENTER_VERTICAL | Gravity.LEFT);
  }

  public void setRowsGravityRight() {
    setRowsGravity(Gravity.CENTER_VERTICAL | Gravity.RIGHT);
  }

  private void setRowsGravity(int gravity) {
    mLinearLayout.setGravity(gravity);
  }

  public void setFieldPrimary(String val) {
    mPrimary.setText(val);
  }

  public void setFieldSecondary(String val) {
    mSecondary.setText(val);
  }

  public boolean isEditable() {
    return false;
  }

  protected int getLayout() {
    return R.layout.currency_item;
  }

}
