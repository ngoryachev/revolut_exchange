package com.revolut.app.feature.exchange.model;

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory;

import java.util.concurrent.TimeUnit;

import io.reactivex.Observable;
import io.reactivex.android.schedulers.AndroidSchedulers;
import io.reactivex.schedulers.Schedulers;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class ExchangeService {

  public static final int POLL_PERIOD_SECS = 30;

  private final FixerService mFixerService;

  public ExchangeService() {
    Retrofit retrofit = new Retrofit.Builder()
        .baseUrl("http://api.fixer.io/")
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create())
        .build();

    mFixerService = retrofit.create(FixerService.class);
  }

  public Observable<CurrencyRates> pollCurrencyChanges(String currencyName) {
    Observable<CurrencyRates> currency = mFixerService.getCurrencyRates(currencyName).retry();
    return currency.mergeWith(Observable.interval(POLL_PERIOD_SECS, TimeUnit.SECONDS)
        .flatMap(number -> currency))
        .distinctUntilChanged()
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread());
  }

}
