package com.revolut.app.feature.exchange.model;

import com.revolut.app.core.Utils;
import com.revolut.app.feature.exchange.Contract;

import java.util.Observer;

public class ExchangeModel implements Contract.Model {

  private final Wallet mWallet = new Wallet();
  private final CurrencyRatesContainer mCurrencyRatesContainer = new CurrencyRatesContainer();
  private final ExchangeService mExchangeService = new ExchangeService();
  private final java.util.Observable mCurrencyChangedObservable = new Utils.SimpleObservable();

  public ExchangeModel() {
    startPollingCurrencyRates();
  }

  private void startPollingCurrencyRates() {
    pollCurrencyUdateContainerAndNotifyChanged(CurrencyHelper.EUR_CODE);
    pollCurrencyUdateContainerAndNotifyChanged(CurrencyHelper.USD_CODE);
    pollCurrencyUdateContainerAndNotifyChanged(CurrencyHelper.GBP_CODE);
  }

  private void pollCurrencyUdateContainerAndNotifyChanged(String currencyName) {
    mExchangeService.pollCurrencyChanges(currencyName)
        .subscribe(rates -> {
          mCurrencyRatesContainer.setCurrencyRates(currencyName, rates);
          mCurrencyChangedObservable.notifyObservers(currencyName);
        });
  }

  @Override
  public Double getWalletCurrencyAmount(String currencyName) {
    return mWallet.getAmount(currencyName);
  }

  @Override
  public Double getCurrencyExchangeRate(String fromCurrencyCode, String toCurrencyCode) {
    CurrencyRates currencyRates = mCurrencyRatesContainer.getCurrencyRates(fromCurrencyCode);
    if (currencyRates != null) {
      return currencyRates.getCurrencyRateByName(toCurrencyCode);
    }

    return null;
  }

  @Override
  public void subscribeOnCurrencyRatesChanged(Observer observer) {
    mCurrencyChangedObservable.addObserver(observer);
  }

  @Override
  public void unSubscribeOnCurrencyRatesChanged(Observer observer) {
    mCurrencyChangedObservable.deleteObserver(observer);
  }

}
