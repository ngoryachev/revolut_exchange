package com.revolut.app.core;

public interface BasePresenter<V> {

  V getView();

  void start();

  void stop();

}
