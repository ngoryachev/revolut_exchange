package com.revolut.app.feature.exchange.model;

import android.support.annotation.NonNull;
import android.support.annotation.Nullable;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.Map;

public class CurrencyRates {

  @SerializedName("base")
  @Expose
  private String base;
  @SerializedName("date")
  @Expose
  private String date;
  @SerializedName("rates")
  @Expose
  private Map<String, Double> rates;

  public String getBase() {
    return base;
  }

  public void setBase(String base) {
    this.base = base;
  }

  public String getDate() {
    return date;
  }

  public void setDate(String date) {
    this.date = date;
  }

  public Map<String, Double> getRates() {
    return rates;
  }

  public void setRates(Map<String, Double> rates) {
    this.rates = rates;
  }

  @Nullable public Double getCurrencyRateByName(@NonNull String name) {
    if (this.rates != null) {
      if (name.equals(base)) {
        return 1.0;
      } else {
        return this.rates.get(name);
      }
    }

    return null;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (o == null || getClass() != o.getClass()) return false;

    CurrencyRates that = (CurrencyRates) o;

    if (base != null ? !base.equals(that.base) : that.base != null) return false;
    if (date != null ? !date.equals(that.date) : that.date != null) return false;
    return rates != null ? rates.equals(that.rates) : that.rates == null;

  }

  @Override
  public int hashCode() {
    int result = base != null ? base.hashCode() : 0;
    result = 31 * result + (date != null ? date.hashCode() : 0);
    result = 31 * result + (rates != null ? rates.hashCode() : 0);
    return result;
  }

  @Override
  public String toString() {
    return "CurrencyRates{" +
        "base='" + base + '\'' +
        ", date='" + date + '\'' +
        ", rates={USD=" + getCurrencyRateByName(CurrencyHelper.USD_CODE) +
        ", EUR=" + getCurrencyRateByName(CurrencyHelper.EUR_CODE) +
        ", GBP=" + getCurrencyRateByName(CurrencyHelper.GBP_CODE) +
        "}}";
  }
}