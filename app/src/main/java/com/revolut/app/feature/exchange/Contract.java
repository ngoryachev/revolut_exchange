package com.revolut.app.feature.exchange;

import android.os.Bundle;

import com.revolut.app.core.BasePresenter;
import com.revolut.app.core.BaseView;

import java.util.Observer;

public interface Contract {

  interface Model {

    Double getWalletCurrencyAmount(String currencyName);

    Double getCurrencyExchangeRate(String fromCurrencyCode, String toCurrencyCode);

    void subscribeOnCurrencyRatesChanged(Observer observer);

    void unSubscribeOnCurrencyRatesChanged(Observer observer);
  }

  interface Presenter extends BasePresenter<Contract.View> {

    void restoreState(Bundle bundle);

    void storeState(Bundle bundle);

    void onInputIndexChanged(int index);

    void onOutputIndexChanged(int index);

    void onInputAmountChanged(String input);

    int getPagesCount();

    String getInputCurrencyCode();

    Double getInputValue();

    Double getInputValueForCurrencyCode(String currencyCode);

    Double getUserBalance(String currencyCode);

    Double getCurrencyExchangeRate(String fromCurrencyCode, String toCurrencyCode);

    String getCurrencyCodeForPosition(int position);
  }

  interface View extends BaseView<Contract.Presenter>{

    void updateOutputView();

    void subscribeInputChanged(Observer observer);

    void unsubscribeInputChanged(Observer observer);

    void showBadInputNotificationText();

  }

}
