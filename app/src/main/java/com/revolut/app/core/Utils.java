package com.revolut.app.core;

import java.util.Observable;

public abstract class Utils {

  public static class SimpleObservable extends Observable {

    @Override
    public void notifyObservers(Object data) {
      setChanged();
      super.notifyObservers(data);
      setChanged();
    }
  }

  public static int indexOf(Object[] array, Object o) {
    if (o == null) {
      for (int i = 0; i < array.length; i++)
        if (array[i] == null)
          return i;
    } else {
      for (int i = 0; i < array.length; i++)
        if (o.equals(array[i]))
          return i;
    }
    return -1;
  }

}
