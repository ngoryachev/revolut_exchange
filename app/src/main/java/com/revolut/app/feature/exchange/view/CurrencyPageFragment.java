package com.revolut.app.feature.exchange.view;


import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.revolut.app.R;
import com.revolut.app.feature.exchange.Contract;

import java.text.DecimalFormat;
import java.text.DecimalFormatSymbols;
import java.text.NumberFormat;
import java.util.Currency;
import java.util.Locale;
import java.util.Observer;

import hugo.weaving.DebugLog;

public class CurrencyPageFragment extends Fragment{

  public static final String INPUT_ENABLED = "inputEnabled";
  public static final String CURRENCY_CODE = "position";

  private static final DecimalFormat mDecimalFormat = new DecimalFormat("#.##",
      DecimalFormatSymbols.getInstance( Locale.ENGLISH ));

  protected CurrencyItemView mItemViewLeft;
  protected CurrencyItemView mItemViewRight;

  private Observer mInputChangedObserver = (observable, data) -> bindView();

  public static CurrencyPageFragment create(boolean inputEnabled, String currencyCode) {
    CurrencyPageFragment currencyPageFragment = inputEnabled
        ? new CurrencyPageEditableFragment()
        : new CurrencyPageFragment();

    Bundle args = new Bundle();
    args.putBoolean(INPUT_ENABLED, inputEnabled);
    args.putString(CURRENCY_CODE, currencyCode);
    currencyPageFragment.setArguments(args);

    return currencyPageFragment;
  }

  protected boolean isInputEnabled() {
    return getArguments().getBoolean(INPUT_ENABLED);
  }

  protected String getCurrencyCode() {
    return getArguments().getString(CURRENCY_CODE);
  }

  protected Contract.View getView_() {
    return Contract.View.class.cast(getContext());
  }

  protected Contract.Presenter getPresenter() {
    return Contract.View.class.cast(getContext()).getPresenter();
  }

  @Override
  public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
    ViewGroup rootView = onCreateViewBasic(inflater, container);

    getView_().subscribeInputChanged(mInputChangedObserver);

    bindView();

    return rootView;
  }

  @NonNull
  protected ViewGroup onCreateViewBasic(LayoutInflater inflater, ViewGroup container) {
    ViewGroup rootView = (ViewGroup) inflater.inflate(getLayout(), container, false);

    mItemViewLeft = (CurrencyItemView) rootView.getChildAt(0);
    mItemViewRight = (CurrencyItemView) rootView.getChildAt(1);

    mItemViewLeft.setRowsGravityLeft();
    mItemViewRight.setRowsGravityRight();
    return rootView;
  }

  @Override
  public void onDestroyView() {
    getView_().unsubscribeInputChanged(mInputChangedObserver);

    super.onDestroyView();
  }

  protected int getLayout() {
    return R.layout.currency_layout;
  }

  protected void bindView() {
    bindLeftSide();

    String currencyCode = getCurrencyCode();
    String inputCurrencyCode = getPresenter().getInputCurrencyCode();
    Double inputValue = getPresenter().getInputValue();
    Double exchangeRate = getPresenter().getCurrencyExchangeRate(inputCurrencyCode, currencyCode);

    mItemViewRight.setFieldPrimary(formatOutputAmount(inputValue, exchangeRate));
    mItemViewRight.setFieldSecondary(
        formatCurrencyExchangeRate(
            inputCurrencyCode,
            currencyCode,
            exchangeRate
        )
    );
  }

  @NonNull
  protected void bindLeftSide() {
    String currencyCode = getCurrencyCode();
    mItemViewLeft.setFieldPrimary(currencyCode);
    mItemViewLeft.setFieldSecondary(formatCurrencyBalance(currencyCode, getPresenter().getUserBalance(currencyCode)));
  }

  private String formatOutputAmount(@Nullable Double inputValue, @Nullable Double exchangeRate) {
    if (inputValue == null || exchangeRate == null) {
      return "";
    }

    return formatCurrencyValue(inputValue * exchangeRate);
  }

  @DebugLog
  protected String formatCurrencyValue(@Nullable Double inputValue) {
    return inputValue != null ? mDecimalFormat.format(inputValue) : "";
  }

  protected String formatCurrencyBalance(@NonNull String currencyCode, @NonNull Double balance) {
    NumberFormat format = NumberFormat.getInstance();
    Currency currency = Currency.getInstance(currencyCode);
    format.setCurrency(currency);
    String result = format.format(balance) + currency.getSymbol();
    return getString(R.string.you_have_amount, result);
  }

  protected String formatCurrencyExchangeRate(@NonNull String fromCurrencyCode, @NonNull String toCurrencyCode, @Nullable Double exchangeRate) {
    if (exchangeRate != null) {
      Currency fromCurrency = Currency.getInstance(fromCurrencyCode);
      Currency toCurrency = Currency.getInstance(toCurrencyCode);
      return String.format(Locale.getDefault(), "%d%s=%.3f%s", 1, fromCurrency.getSymbol(), exchangeRate, toCurrency.getSymbol());
    }

    return "";
  }

}
