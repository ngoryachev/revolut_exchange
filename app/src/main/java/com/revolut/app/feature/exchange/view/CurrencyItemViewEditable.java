package com.revolut.app.feature.exchange.view;

import android.content.Context;
import android.text.InputFilter;
import android.util.AttributeSet;
import android.widget.EditText;

import com.revolut.app.R;

import hugo.weaving.DebugLog;

public class CurrencyItemViewEditable extends CurrencyItemView {

  private EditText mInput;

  public CurrencyItemViewEditable(Context context) {
    super(context);
    initialize();
  }

  public CurrencyItemViewEditable(Context context, AttributeSet attrs) {
    super(context, attrs);
    initialize();
  }

  public CurrencyItemViewEditable(Context context, AttributeSet attrs, int defStyleAttr) {
    super(context, attrs, defStyleAttr);
    initialize();
  }

  private void initialize() {
    mInput = (EditText) mPrimary;
    InputFilter filter = (source, start, end, dest, dstart, dend) ->
        toTrimmedString(source.toString());
    mInput.setFilters(new InputFilter[] { filter });
  }

  @DebugLog
  private String toTrimmedString(String original) {
    return original.replaceAll("[^\\d\\.]", "");
  }

  public EditText getInput() {
    return mInput;
  }

  @Override
  public boolean isEditable() {
    return true;
  }

  protected int getLayout() {
    return R.layout.currency_item_editable;
  }
}
