package com.revolut.app;

import android.app.Application;

import com.revolut.app.feature.exchange.Contract;
import com.revolut.app.feature.exchange.model.ExchangeModel;

public class App extends Application {

  private ExchangeModel mExchangeModel;

  @Override
  public void onCreate() {
    super.onCreate();

    mExchangeModel = new ExchangeModel();
  }

  public Contract.Model getExchangeModel() {
    return mExchangeModel;
  }
}
